<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE concept PUBLIC "-//OASIS//DTD DITA Concept//EN" "concept.dtd">

<concept id="configuring_data_collection__datapower_soa_appliance" audience="expert novice all" importance="recommended">
    <title>Configuring Data Collection: DataPower SOA Appliance</title>

   
    <shortdesc>Support for monitoring of service flows through an IBM®
        WebSphere® DataPower® <abbreviated-form keyref="SOA"/> appliance, where the ITCAM for SOA data collector
        acts as a proxy between the services clients and servers.</shortdesc>
   <prolog>
       <metadata>
           <keywords>
               <indexterm id="index_ITCAM">ITCAM</indexterm>
               <indexterm id="index_DataPowerSOAappliances">DataPower SOA appliances</indexterm>
               <indexterm id="index_TheWebServicesProxy">The Web Services Proxy</indexterm>
               <indexterm id="index_TheMulti-ProtocolGateway">The Multi-Protocol Gateway</indexterm>
           </keywords>
       </metadata>
   </prolog>
    <conbody>

        <p>The list of versions of the DataPower SOA appliance supported by ITCAM for
            SOA 7.2 Fix Pack 1 is available from the Software product compatibility reports
            website. </p>
        <p>The DataPower data collector can be integrated with ITCAM for Transactions. If
            configured, transaction events are sent to a Transaction Collector which stores and
            aggregates transaction data from multiple data collectors. For more information
            about configuring the interface to ITCAM for Transactions, see Integrating with
            ITCAM for Transactions.</p>
        <p>IBM WebSphere DataPower SOA appliances are used for processing <abbreviated-form keyref="XML"/>
            messages, and providing message transformation, services acceleration, and
            security functions. A DataPower appliance is typically used to improve the security
            and performance of services by offloading functions from the application server
            that is hosting the target service to a DataPower SOA appliance. Typical functions
            that are off-loaded include; authentication and authorization, XML schema
            validation, and services encryption and decryption.</p>
        <p>ITCAM for SOA provides a DataPower data collector that operates as a proxy and
            monitors services flows through a DataPower SOA appliance, providing similar
            services management and availability information that ITCAM for SOA currently
            provides for application server runtime environments. This information is
            displayed in the <abbreviated-form keyref="Tivoli"/> using the usual predefined or
            user-defined workspaces and views.</p>
        <p>DataPower supports two proxy types that can process SOA messages:</p>
        
       
        <dl>
            <dlentry>
                <dt>The Web Services Proxy</dt>
                <dd>You configure a Web Services Proxy by importing one or more WSDL files
                    and then telling the appliance where to direct those messages. Thus, the
                    Web Services Proxy receives only SOAP messages.</dd>
            </dlentry>
            <dlentry>
                <dt>The Multi-Protocol Gateway</dt>
                <dd>The Multi-Protocol Gateway is more versatile than the Web Services Proxy.
                    You can use it to process nearly any type of message, including SOAP,
                    non-SOAP XML, text, or binary. For XML messages (including SOAP), XSL
                    transforms are used to manipulate the message. For non-XML messages,
                    similar transform actions can be built using IBM WebSphere
                    Transformation Extender.</dd>
            </dlentry>
        </dl>
      <note conref="notes_warnings.dita#notes_warnings/Upgrade"></note>
      <note conref="notes_warnings.dita#notes_warnings/UpgradeTo3.7.1WSP"></note>
        <note conref="notes_warnings.dita#notes_warnings/UpgradeTo3.7.1MPG"></note>
       
    </conbody>
    
</concept>
