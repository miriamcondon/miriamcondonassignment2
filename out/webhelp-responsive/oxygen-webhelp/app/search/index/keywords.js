define(function() {var keywords=[{w:"MPG",p:["p0"]},{w:"SOA",p:["p1","p6","p9","p32","p34"]},{w:"Tivoli\u00AE",p:["p2"]},{w:"Enterprise",p:["p2","p12","p13"]},{w:"Portal",p:["p2","p12","p13"]},{w:"WSP",p:["p3"]},{w:"XML",p:["p4","p35","p36"]},{w:"ITCAM",p:["p5"]},{w:"DataPower",p:["p6","p9","p10","p11","p22","p26","p29","p32","p34","p37","p44"]},{w:"appliances",p:["p6"]},{w:"The",p:["p7","p8","p22","p26","p28","p29","p32","p34","p36","p37","p39","p40","p41","p42","p44","p45"]},{w:"Web",p:["p7"]},{w:"Services",p:["p7"]},{w:"Proxy",p:["p7","p27","p28","p29"]},{w:"Multi-Protocol",p:["p8"]},{w:"Gateway",p:["p8"]},{w:"Configuring",p:["p9","p31","p32","p33","p34","p36"]},{w:"Data",p:["p9","p11","p14","p15","p17","p22","p28","p29","p37","p39","p40","p41","p42","p44","p45"]},{w:"Collection:",p:["p9"]},{w:"Appliance",p:["p9","p32","p34"]},{w:"Monitoring",p:["p10","p11","p33","p34"]},{w:"Considerations",p:["p11"]},{w:"for",p:["p11","p21","p33","p34"]},{w:"enabling",p:["p11","p16","p17","p40","p41"]},{w:"collection",p:["p11","p14","p15","p17","p39","p40","p41"]},{w:"Tivoli",p:["p12","p13"]},{w:"Creating",p:["p13"]},{w:"node",p:["p13"]},{w:"names",p:["p13"]},{w:"in",p:["p13"]},{w:"Disabling",p:["p15","p38","p39"]},{w:"Optimizing",p:["p18"]},{w:"performance",p:["p18"]},{w:"Deployment",p:["p19","p21"]},{w:"Planning",p:["p20","p21"]},{w:"Running",p:["p22"]},{w:"collector",p:["p22","p29","p37","p39","p40","p42","p44","p45"]},{w:"as",p:["p22","p28","p29"]},{w:"a",p:["p22","p28","p29","p31","p32"]},{w:"Windows",p:["p22"]},{w:"service",p:["p22"]},{w:"or",p:["p22"]},{w:"UNIX",p:["p22"]},{w:"daemon",p:["p22"]},{w:"KD4configDC",p:["p23","p25","p41"]},{w:"Specifying",p:["p24","p25"]},{w:"Parameters",p:["p24","p25"]},{w:"Configuration",p:["p26","p39","p40"]},{w:"File",p:["p26"]},{w:"Power",p:["p28"]},{w:"User",p:["p30","p31","p32"]},{w:"Account",p:["p30","p31","p32"]},{w:"on",p:["p32"]},{w:"Interface",p:["p35","p36"]},{w:"Management",p:["p36"]},{w:"Deploying",p:["p37"]},{w:"using",p:["p39","p40","p41"]},{w:"utility",p:["p39","p40"]},{w:"command",p:["p41"]},{w:"Starting",p:["p42"]},{w:"and",p:["p42"]},{w:"Stopping",p:["p42"]},{w:"Unconfiguring",p:["p43","p44"]},{w:"Upgrading",p:["p45"]}];
var ph={};
ph["p0"]=[0];
ph["p1"]=[1];
ph["p2"]=[2, 3, 4];
ph["p3"]=[5];
ph["p4"]=[6];
ph["p5"]=[7];
ph["p6"]=[8, 1, 9];
ph["p7"]=[10, 11, 12, 13];
ph["p8"]=[10, 14, 15];
ph["p9"]=[16, 17, 18, 8, 1, 19];
ph["p30"]=[50, 51];
ph["p10"]=[8, 20];
ph["p32"]=[16, 38, 50, 51, 52, 10, 8, 1, 19];
ph["p31"]=[16, 38, 50, 51];
ph["p12"]=[25, 3, 4];
ph["p34"]=[16, 10, 8, 1, 19, 22, 20];
ph["p11"]=[21, 22, 23, 17, 24, 22, 8, 20];
ph["p33"]=[16, 22, 20];
ph["p14"]=[17, 24];
ph["p36"]=[16, 10, 6, 54, 53];
ph["p13"]=[26, 27, 28, 29, 25, 3, 4];
ph["p35"]=[6, 53];
ph["p16"]=[23];
ph["p38"]=[30];
ph["p15"]=[30, 17, 24];
ph["p37"]=[55, 10, 8, 17, 36];
ph["p18"]=[31, 32];
ph["p17"]=[23, 17, 24];
ph["p39"]=[30, 17, 24, 56, 10, 17, 36, 47, 57];
ph["p19"]=[33];
ph["p41"]=[23, 17, 24, 56, 10, 44, 58];
ph["p40"]=[23, 17, 24, 56, 10, 17, 36, 47, 57];
ph["p21"]=[34, 22, 33];
ph["p43"]=[62];
ph["p20"]=[34];
ph["p42"]=[59, 60, 61, 10, 17, 36];
ph["p23"]=[44];
ph["p45"]=[63, 10, 17, 36];
ph["p22"]=[35, 10, 8, 17, 36, 37, 38, 39, 40, 41, 42, 43];
ph["p44"]=[62, 10, 8, 17, 36];
ph["p25"]=[45, 44, 46];
ph["p24"]=[45, 46];
ph["p27"]=[13];
ph["p26"]=[10, 8, 47, 48];
ph["p29"]=[10, 8, 17, 36, 37, 38, 13];
ph["p28"]=[10, 17, 49, 37, 38, 13];
     return {
         keywords: keywords,
         ph: ph
     }
});
